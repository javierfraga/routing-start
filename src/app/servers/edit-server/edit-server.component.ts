import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Params,Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import { ServersService } from '../servers.service';
import {CanComponentDeactivate} from './can-deactivate-guard.service';

@Component({
  selector: 'app-edit-server',
  templateUrl: './edit-server.component.html',
  styleUrls: ['./edit-server.component.css']
})
export class EditServerComponent implements OnInit,CanComponentDeactivate {
  server: {id: number, name: string, status: string};
  serverName = '';
  serverStatus = '';
  allowEdit = false;
  changesSaved = false;

  constructor(
    private serversService: ServersService,
    private ActivatedRoute:ActivatedRoute,
    private Router:Router,
  ) { }

  ngOnInit() {
    console.log(this.ActivatedRoute.snapshot.queryParams);
    console.log(this.ActivatedRoute.snapshot.fragment);
    this.ActivatedRoute.queryParams.subscribe(
      (Params:Params)=>{
        this.allowEdit = Params['allowEdit'] === '1'?true:false;
      }
    )
    const id = +this.ActivatedRoute.snapshot.params['id'];
    this.server = this.serversService.getServer(id);
    this.serverName = this.server.name;
    this.serverStatus = this.server.status;
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.allowEdit) {
      return true;
    }
    if ((this.serverName!==this.server.name || this.serverStatus !== this.server.status)&&!this.changesSaved) {
      return confirm('Do you want to discard the chaanges?');
    } else {
      return true;
    }
  }

  onUpdateServer() {
    this.serversService.updateServer(this.  server.id, {name: this.serverName, status: this.serverStatus});
    this.changesSaved = true;
    this.Router.navigate(['../'],{relativeTo:this.ActivatedRoute})
  }

}
