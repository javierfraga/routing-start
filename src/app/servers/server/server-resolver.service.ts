import {Resolve,ActivatedRouteSnapshot,RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {ServersService} from '../servers.service';


interface Server {
	id:number,
	name:string,
	status:string,
}

@Injectable()
export class ServerResolver implements Resolve<Server> {

	constructor(
		private ServersService:ServersService,
	){}

	resolve(
		ActivatedRouteSnapshot:ActivatedRouteSnapshot,
		RouterStateSnapshot:RouterStateSnapshot
	): Observable<Server> | Promise<Server> | Server {
		return this.ServersService.getServer(+ActivatedRouteSnapshot.params['id'])
	}
}