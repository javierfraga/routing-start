import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Params,Router,Data} from '@angular/router';

import { ServersService } from '../servers.service';

@Component({
	selector: 'app-server',
	templateUrl: './server.component.html',
	styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
	server: {id: number, name: string, status: string};

	constructor(
		private serversService: ServersService,
		private ActivatedRoute:ActivatedRoute, 
		private Router:Router, 
		) { }

	ngOnInit() {
	  	// whoa, look at this casting from string number to actual number
	 //  	const id = +this.ActivatedRoute.snapshot.params['id'];
	 //  	this.server = this.serversService.getServer(id);
	 //  	this.ActivatedRoute.params.subscribe(
	 //  		(Params:Params)=>{
	 //  			this.server = this.serversService.getServer(+Params['id']);
	 //  		}
		// )
		this.ActivatedRoute.data.subscribe(
			(Data:Data)=>{
				this.server = Data.ServerResolver 
			}
		)
	}
	editServer() {
		this.Router.navigate(
			['edit'], 
			{
				relativeTo:this.ActivatedRoute,
				// can use 'merge' instead which will not overwrite new values
				// 'preserve' does overwrite new param values
				queryParamsHandling:'preserve',
			}
		);
	}

}
