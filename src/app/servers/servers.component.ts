import { Component, OnInit } from '@angular/core';
import { ServersService } from './servers.service';
import {Router,ActivatedRoute} from '@angular/router';

@Component({
	selector: 'app-servers',
	templateUrl: './servers.component.html',
	styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
	private servers: {id: number, name: string, status: string}[] = [];

	constructor(
		private serversService: ServersService,
		private Router: Router,
		private ActivatedRoute: ActivatedRoute,
	) { }

	ngOnInit() {
		this.servers = this.serversService.getServers();
	}

	reload() {
		/*
		 * this sets it to relative path
		 */
		// this.Router.navigate(['servers'], {relativeTo: this.ActivatedRoute});
		/*
		 * this sets it to absolute path, just they way it works
		 */
		this.Router.navigate(['servers']);
	}

}
