import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Data} from '@angular/router';

@Component({
	selector: 'app-error-page',
	templateUrl: './error-page.component.html',
	styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {
	errorMessage:string;

	constructor(private ActivatedRoute:ActivatedRoute) { }

	ngOnInit() {
		this.errorMessage = this.ActivatedRoute.snapshot.data.message;
		this.ActivatedRoute.data.subscribe(
			(Data:Data)=>{
				this.errorMessage = Data.message;
			}
		)
	}

}
