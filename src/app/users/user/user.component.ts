import { Component, OnInit,OnDestroy } from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute,Params} from '@angular/router';

@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit,OnDestroy {
	user: {id: number, name: string};
	paramSubsciption:Subscription;

	constructor(private ActivatedRoute:ActivatedRoute) { }

	ngOnInit() {
		this.user = {
			id: this.ActivatedRoute.snapshot.params['id'],
			name: this.ActivatedRoute.snapshot.params['name'],
		}
		this.paramSubsciption = this.ActivatedRoute.params.subscribe(
			(Params:Params)=>{
				this.user.id = Params['id'];
				this.user.name = Params['name'];
			}
			)
	}
	ngOnDestroy() {
		this.paramSubsciption.unsubscribe();
	}

}
