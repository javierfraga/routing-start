import {CanActivate,CanActivateChild,ActivatedRouteSnapshot,RouterStateSnapshot,Router} from '@angular/router'
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate,CanActivateChild {
	constructor(
		private authService:AuthService,
		private Router:Router,
	) {}

	canActivate(
		ActivatedRouteSnapshot:ActivatedRouteSnapshot,
		RouterStateSnapshot:RouterStateSnapshot,
	): Observable<boolean> | Promise<boolean> | boolean {
		return this.authService.isAuthenticated()
		.then(
			(authenticated:boolean)=>{
				if (authenticated) { 
					return true;
				} else {
					this.Router.navigate(['/']);
				}
			}
		)
	}
	canActivateChild(
		ActivatedRouteSnapshot:ActivatedRouteSnapshot,
		RouterStateSnapshot:RouterStateSnapshot,
	): Observable<boolean> | Promise<boolean> | boolean {
		return this.canActivate(ActivatedRouteSnapshot,RouterStateSnapshot);
	}
}